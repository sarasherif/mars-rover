class Rover {
    x: number;
    y: number;
    direction: string;

    constructor(x: number, y: number, direction: string) {
        this.x = x;
        this.y = y;
        this.direction = direction;

    }

    move(commands: string) {
        const individualCommands = commands.split("");
        this.executeCommand(individualCommands);
    }

    turnLeft(direction: string) {
        switch (direction) {
            case "N":
                this.direction = "W"
                break;
            case "S":
                this.direction = "E"
                break;
            case "E":
                this.direction = "N"
                break;
            case "W":
                this.direction = "S"
                break;
        }
    }

    turnRight(direction: string) {
        switch (direction) {
            case "N":
                this.direction = "E"
                break;
            case "S":
                this.direction = "W"
                break;
            case "E":
                this.direction = "S"
                break;
            case "W":
                this.direction = "N"
                break;
        }
    }
    executeCommand(commands: string[]) {
        commands.forEach(command => {
            (command === "F" && this.direction === 'N') && this.y--;
            (command === "F" && this.direction === 'S') && this.y++;
            (command === "F" && this.direction === 'W') && this.x--;
            (command === "F" && this.direction === 'E') && this.x++;
            (command === "B" && this.direction === 'N') && this.y++;
            (command === "B" && this.direction === 'S') && this.y--;
            (command === "B" && this.direction === 'W') && this.x++;
            (command === "B" && this.direction === 'E') && this.x--;
            command === "L" && this.turnLeft(this.direction);
            command === "R" && this.turnRight(this.direction);

        });
    }
}
export default Rover;