import anyTest, { TestInterface } from 'ava';
import { expect } from 'chai';
import Rover from '../src/rover-kata';

const test = anyTest as TestInterface<{ rover: Rover }>;

test.beforeEach((t) => {
    t.context.rover = new Rover(0, 0, 'N');
});

test("should create the rover instance", (t) => {
    expect(t.context.rover).to.be.an.instanceof(Rover);
    t.pass();
});

test("should create a rover with the given initial position", (t) => {
    expect(t.context.rover.x).to.be.eql(0);
    expect(t.context.rover.y).to.be.eql(0);
    t.pass();

});

test("should create a rover with the given initial direction", (t) => {
    expect(t.context.rover.direction).to.be.eql('N');
    t.pass();

});

test("should reduce Y when moving north", (t) => {
    t.context.rover.move('F');

    expect(t.context.rover.y).to.be.eql(-1);
    t.pass();

});

test("should increase Y when moving south", (t) => {
    t.context.rover = new Rover(0, 0, 'S');
    t.context.rover.move('F');

    expect(t.context.rover.y).to.be.eql(1);
    t.pass();

});

test("should reduce X when moving west", (t) => {
    t.context.rover = new Rover(0, 0, 'W');
    t.context.rover.move('F');

    expect(t.context.rover.x).to.be.eql(-1);
    t.pass();

});

test("should increase X when moving east", (t) => {
    t.context.rover = new Rover(0, 0, 'E');
    t.context.rover.move('F');

    expect(t.context.rover.x).to.be.eql(1);
    t.pass();

});

test("should increase Y when moving backwards facing north", (t) => {
    t.context.rover = new Rover(0, 0, 'N');
    t.context.rover.move('B');

    expect(t.context.rover.y).to.be.eql(1);
    t.pass();

});

test("should reduce Y when moving backwards facing south", (t) => {
    t.context.rover = new Rover(0, 0, 'S');
    t.context.rover.move('B');

    expect(t.context.rover.y).to.be.eql(-1);
    t.pass();

});

test("should increase X when moving backwards facing west", (t) => {
    t.context.rover = new Rover(0, 0, 'W');
    t.context.rover.move('B');

    expect(t.context.rover.x).to.be.eql(1);
    t.pass();

});

test("should reduce X when moving backwards facing east", (t) => {
    t.context.rover = new Rover(0, 0, 'E');
    t.context.rover.move('B');

    expect(t.context.rover.x).to.be.eql(-1);
    t.pass();

});

test("should change direction from E to N when command is to turn left", (t) => {
    t.context.rover = new Rover(0, 0, 'E');
    t.context.rover.move('L');

    expect(t.context.rover.direction).to.be.eql('N');
    t.pass();

});

test("should change direction from N to W when command is to turn left", (t) => {
    t.context.rover.move('L');

    expect(t.context.rover.direction).to.be.eql('W');
    t.pass();

});

test("should change direction from E to S when command is to turn right", (t) => {
    t.context.rover = new Rover(0, 0, 'E');
    t.context.rover.move('R');

    expect(t.context.rover.direction).to.be.eql('S');
    t.pass();

});

test("should execute command string that contains multiple commands", (t) => {
    t.context.rover.move('​FLFFFRFLBR');

    expect(t.context.rover.x).to.be.eql(-2);
    expect(t.context.rover.y).to.be.eql(-2);
    expect(t.context.rover.direction).to.be.eql('N');
    t.pass();

});